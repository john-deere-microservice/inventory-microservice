package com.classpath.inventoryservice.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/inventory")
public class InventoryController {

    private static int counter  = 1000;

    @PostMapping("/{count}")
    public int updateIntentory(@PathVariable int count){
        System.out.println(" Came inside the inventory service update Inventory method ::");
        System.out.println(" Came inside the inventory service update Inventory method ::");
        System.out.println(" Came inside the inventory service update Inventory method ::");
        counter = counter - count;
        return counter;
    }
}